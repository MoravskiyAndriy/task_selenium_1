package com.moravskiyandriy.pagechecker;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class PageChecker {
    private WebDriver driver;

    public PageChecker(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        driver=new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    public boolean elementPresent(){
        boolean pageSuccessfullyLoadedFlag;
        driver.get("https://www.google.com");
        WebElement element = driver.findElement(By.name("q"));
        element.clear();
        element.sendKeys("Apples");
        element.submit();
        driver.findElement(By.className("qs")).click();
        try{
            element=driver.findElement(By.id("r02MU2ZZ1B2QWM:"));
            pageSuccessfullyLoadedFlag=(Boolean) ((JavascriptExecutor)driver).executeScript(
                    "return arguments[0].complete && typeof arguments[0].naturalWidth != " +
                            "\"undefined\" && arguments[0].naturalWidth > 0", element);
            driver.quit();
        }catch (TimeoutException e){
            System.out.println("TimeoutException");
            pageSuccessfullyLoadedFlag=false;
            driver.quit();
        }
        catch (ElementNotVisibleException e){
            System.out.println("ElementNotVisibleException");
            pageSuccessfullyLoadedFlag=false;
            driver.quit();
        }
        catch (NoSuchElementException e){
            System.out.println("NoSuchElementException");
            pageSuccessfullyLoadedFlag=false;
            driver.quit();
        }
        return pageSuccessfullyLoadedFlag;
    }
}
